import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //get player => null;



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                getMusicButton(Colors.green,1),
                getMusicButton(Colors.greenAccent,2),
                getMusicButton(Colors.limeAccent,3),
                getMusicButton(Colors.blue,4),
                getMusicButton(Colors.black,5),
                getMusicButton(Colors.redAccent,6),
                getMusicButton(Colors.deepPurple,7),
              ],
            ),
          ),
        ));
  }
  Expanded getMusicButton(Color color ,int soundNumber){
    return Expanded(
      child: Container(
        color: color,
        child: InkWell(
          onTap: () {
            playMusic(soundNumber);
          },
        ),
      ),
    );
  }
  void playMusic(soundNumber){
     AudioPlayer player = AudioPlayer();
     player.play(AssetSource('audio/assets_note1$soundNumber.wav'));

  }
}
